# %%
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
import pickle

service = Service(executable_path=ChromeDriverManager().install())
driver = webdriver.Chrome(service=service)
driver.get('https://www.linkedin.com/login/')
input_username = str(input("nhap username: "))
input_password = str(input('nhap password: '))
driver.find_element_by_id('username').send_keys(input_username)
driver.find_element_by_id('password').send_keys(input_password,Keys.ENTER)
url = driver.current_url
if 'checkpoint' in url:
    code = input('insert code:')
    code_input = driver.find_element(By.ID,'input__email_verification_pin').send_keys(code)
    submit_field = driver.find_element(By.ID,'email-pin-submit-button')
    webdriver.ActionChains(driver).click(submit_field).perform()
sleep(4)
pickle.dump(driver.get_cookies(),open('my_cookie.pkl','wb'))

driver.quit()
