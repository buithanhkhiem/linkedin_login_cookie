
from selenium import webdriver

from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

import pickle


service = Service(executable_path=ChromeDriverManager().install())
driver = webdriver.Chrome(service=service)
driver.get('https://www.linkedin.com/login/')



cookie_file = open("my_cookie.pkl","rb")
cookies = pickle.load(cookie_file)
for cookie in cookies:
    driver.add_cookie(cookie)

driver.refresh()


cookies = pickle.load(open("my_cookie.pkl","rb"))



